import * as React from 'react';
import Question from './components/Question';
import Form from './components/Form';

interface Props {}

interface State {
	nextPage: string | null;
	startDesktopImage: string | null;
	startMobileImage: string | null;
}

export default class App extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			nextPage: null,
			startDesktopImage: null,
			startMobileImage: null
		};

		this.handleOptionClick = this.handleOptionClick.bind(this);
		this.handleStartOptionClick = this.handleStartOptionClick.bind(this);
		this.handleStartSubmitClick = this.handleStartSubmitClick.bind(this);
	}

	componentDidMount() {}

	handleOptionClick(option: string) {
		if (option === 'yes') {
			this.setState({ nextPage: 'start' });
		}
		if (option === 'no') {
			this.setState({ nextPage: 'question' });
		}
	}

	handleStartOptionClick(option: string) {
		if (option === 'yes') {
			this.setState({ nextPage: 'question' });
		}
		if (option === 'no') {
			this.setState({ startDesktopImage: null, startMobileImage: null });
		}
	}

	handleStartSubmitClick(values: Array<string>) {
		values.map((value, index) => {
			if (index === 0) {
				this.setState({ startDesktopImage: value });
			} else {
				this.setState({ startMobileImage: value });
			}
			return null;
		});
	}

	render() {
		const { state } = this;
		const { nextPage, startDesktopImage, startMobileImage } = state;

		if (nextPage === null) {
			return (
				<div>
					<Question
						question={'Do you want a Start Page for your Quiz?'}
						options={['yes', 'no']}
						handleOptionClick={this.handleOptionClick.bind(this)}
					/>
				</div>
			);
		}
		if (nextPage === 'start') {
			if (startDesktopImage === null) {
				return (
					<div>
						<Form
							title={'Start Page'}
							types={['input', 'input']}
							labels={['Desktop Image Path', 'Mobile Image Path (optional)']}
							placeholders={['/desktop-image.jpg', '/mobile-image.jpg']}
							handleSubmitClick={this.handleStartSubmitClick.bind(this)}
						/>
					</div>
				);
			} else if (startDesktopImage !== null) {
				let json = {
					stepType: 'start',
					content: ''
				};
				let htmlString = '';

				if (startMobileImage !== null && startMobileImage !== '') {
					htmlString =
						'<picture><source media="(max-width:767px)" srcSet="' +
						startMobileImage +
						'" /><source media="(min-width:768px)" srcSet="' +
						startDesktopImage +
						'" /><img src="' +
						startDesktopImage +
						'" /></picture>';
				} else {
					htmlString = '<img src="' + startDesktopImage + '" />';
				}

				json.content = htmlString;

				return (
					<div>
						<h2>Place the following code in Html 1 field of content asset.</h2>
						<section>{JSON.stringify(json)}</section>
						<Question
							question={'Does this look right?'}
							options={['yes', 'no']}
							handleOptionClick={this.handleStartOptionClick.bind(this)}
						/>
					</div>
				);
			}
		}
		if (nextPage === 'question') {
			return (
				<div>
					<Form
						title={'Question Step'}
						types={['input', 'strings']}
						labels={['Question Text', 'Options']}
						placeholders={['What would you like to know?', 'answer']}
						handleSubmitClick={this.handleStartSubmitClick.bind(this)}
					/>
				</div>
			);
		}
		return <div>Loading...</div>;
	}
}
