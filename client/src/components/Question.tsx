import * as React from 'react';

interface Props {
	question: string;
	options: Array<string>;
	handleOptionClick;
}

interface State {}

export default class Question extends React.Component<Props, State> {
	componentDidMount() {}

	render() {
		const { props } = this;
		const { question, options, handleOptionClick } = props;

		return (
			<div>
				<h2>{question}</h2>
				{options.map((option, index) => {
					return (
						<button key={index} onClick={() => handleOptionClick(option)}>
							{option}
						</button>
					);
				})}
			</div>
		);
	}
}
