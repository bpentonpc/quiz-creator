import * as React from 'react';

interface Props {
	title?: string;
	types: Array<string>;
	labels: Array<string>;
	placeholders: Array<string>;
	handleSubmitClick;
}

interface State {
	inputVals: Array<string>;
}

export default class Input extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			inputVals: ['', '']
		};

		this.handleInputChange = this.handleInputChange.bind(this);
	}

	componentDidMount() {}

	handleInputChange(e, index) {
		let inputs = this.state.inputVals;
		inputs[index] = e.target.value;
		this.setState({ inputVals: inputs });
	}

	render() {
		const { props, state } = this;
		const { types, title, labels, placeholders, handleSubmitClick } = props;
		const { inputVals } = state;

		return (
			<div>
				{title && <h2>{title}</h2>}
				{types.map((type, index) => {
					if (type === 'input') {
						return (
							<div key={index}>
								<label>{labels[index]}: </label>
								<input
									placeholder={placeholders[index]}
									onChange={e => this.handleInputChange(e, index)}
								/>
							</div>
						);
					} else if (type === 'strings') {
						return (
							<div key={index}>
								<label>{labels[index]}: </label>
								<div className={'strings'}>
									<input
										placeholder={placeholders[index]}
										onChange={e => this.handleInputChange(e, index)}
									/>
									<input
										placeholder={placeholders[index]}
										onChange={e => this.handleInputChange(e, index)}
									/>
								</div>
								<button>Add another string</button>
							</div>
						);
					}

					return null;
				})}
				<button onClick={() => handleSubmitClick(inputVals)}>Submit</button>
			</div>
		);
	}
}
